(function ($) {

  Drupal.behaviors.viewsLabelTooltip = {
    attach: function(context) {
      $.each(Drupal.settings.viewsLabelTooltip, function(view, displays) {
        $.each(displays, function(display, settings) {
          $.each(settings.tooltips, function(field, tooltip) {
            $('.view-id-' + view + '.view-display-id-' + display + ' .views-label-tooltip-field-' + field + '.views-field-' + field)
              .once('views-label-tooltip')
              .append(tooltip);
          });
          var $target = $('.views-label-tooltip.views-label-tooltip-' + view + '-' + display + '[title]', '.view-id-' + view + '.view-display-id-' + display);
          var qtip = Drupal.behaviors.viewsLabelTooltip.expandTokens({
            "$target": $target
          }, settings.qtip);
          $target.qtip(qtip); 
        });
      });
    },

    expandTokens: function(tokens, target) {
      for (prop in target) {
        if (target[prop] != null && typeof tokens[target[prop]] !== 'undefined') {
          target[prop] = tokens[target[prop]];
        }
        else if (typeof target[prop].match === 'function' && target[prop].match(/^\$\(.*?\)$/)) {
          target[prop] = eval(target[prop]);
        }
        else if (typeof target[prop] === 'object') {
          target[prop] = Drupal.behaviors.viewsLabelTooltip.expandTokens(tokens, target[prop]);
        }
      }
      return target;
    }
  };

})(jQuery);
