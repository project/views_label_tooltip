<?php

/**
 * @file
 * Views integration for the views_label_tooltip module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_label_tooltip_views_plugins() {
  $path = drupal_get_path('module', 'views_label_tooltip') . '/views';
  $plugins = array();
  $plugins['display_extender']['views_label_tooltip'] = array(
    'title' => t('Views Label Tooltip display extender'),
    'path' => $path,
    'handler' => 'views_label_tooltip_plugin_display_extender',
  );
  return $plugins;
}
