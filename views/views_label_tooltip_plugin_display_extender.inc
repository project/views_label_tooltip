<?php

class views_label_tooltip_plugin_display_extender extends views_plugin_display_extender {
  function options_definition_alter(&$options) {
    $options['tooltips'] = array('default' => array(), 'unpack_translatable' => 'unpack_tooltips');
    $options['qtip'] = array('default' => array());
  }

  function unpack_tooltips(&$translatable, $storage, $option, $definition, $parents, $keys = array()) {
    $tooltips = $storage[$option];
    if (!empty($tooltips)) foreach ($tooltips as $field => $tooltip) {
      $translation_keys = array_merge($keys, array($field));
      $translatable[] = array(
        'value' => $tooltip,
        'keys' => $translation_keys,
        'format' => NULL,
      );
    }
  }

  function options_form(&$form, &$form_state) {
    if ($form_state['section'] != 'qtip') return;

    $form['#title'] = t('Tooltip settings');
    $example = <<<EOS
{
  "position": {
    "my": "top left",
    "at": "bottom right",
    "adjust": {
      "method": "flip"
    }
  },
  "hide": {
    "fixed": true,
    "delay": 300,
    "event": "mouseout"
  },
  "show": {
    "solo": true
  },
  "style": {
    "classes": "qtip qtip-shadow my-custom-class"
  }
}
EOS;
    $form['qtip'] = array(
      '#type' => 'textarea',
      '#title' => t('qTip settings'),
      '#description' => t('
        Add tooltip settings in JSON format as per the <a href="@url" target="_blank">qTip documentation</a>. 
        Leave empty to use qTip defaults.
        Note that the <code>content</code> part will be automatically filled in by each field\'s <strong>Tooltip</strong> setting.
        Here are some common qTip settings:
        <pre>!example</pre>
        The <code><a href="@url_position_my" target="_blank">position.my</a></code> and <code><a href="@url_position_at" target="_blank">position.at</a></code> attributes control the tooltip\'s position relative to the question mark.
        The <code><a href="@url_position_adjustmethod" target="_blank">position.adjust.method</a></code> attribute controls the positioning behavior at the edges of the viewport.
        The <code><a href="@url_hide_fixed" target="_blank">hide.fixed = true</a></code> attribute keeps the tooltip open when moused over.
        The <code><a href="@url_hide_delay" target="_blank">hide.delay</a></code> attribute controls the time in milliseconds by which to delay hiding the tooltip.
        The <code><a href="@url_hide_event" target="_blank">hide.event = "mouseout"</a></code> attribute hides the tooltip when mouse hovers elsewhere.
        The <code><a href="@url_show_solo" target="_blank">show.solo = true</a></code> attribute hides other open tooltips when a new one is activated.
        The <code><a href="@url_style_classes" target="_blank">style.classes = "qtip"</a></code> attribute shows the default qTip tooltip style.
        The <code><a href="@url_style_classes" target="_blank">style.classes = "qtip-shadow"</a></code> attribute adds a drop shadow to the tooltip.
        The <code><a href="@url_style_classes" target="_blank">style.classes = "my-custom-class"</a></code> attribute adds a new CSS class to the tooltip, allowing for user-defined CSS customizations.
        ',
        array(
          '@url' => 'http://qtip2.com/options',
          '!example' => $example,
          '@url_position_my' => 'http://qtip2.com/options#position.my',
          '@url_position_at' => 'http://qtip2.com/options#position.at',
          '@url_position_adjustmethod' => 'http://qtip2.com/plugins#viewport.adjustmethod',
          '@url_hide_fixed' => 'http://qtip2.com/options#hide.fixed',
          '@url_hide_delay' => 'http://qtip2.com/options#hide.delay',
          '@url_hide_event' => 'http://qtip2.com/options#hide.event',
          '@url_show_solo' => 'http://qtip2.com/options#show.solo',
          '@url_style_classes' => 'http://qtip2.com/options#style.classes',
        )
      ),
      '#default_value' => $this->display->get_option('qtip') ? (
        version_compare(PHP_VERSION, '5.4.0') >= 0 ?
          json_encode($this->display->get_option('qtip'), JSON_PRETTY_PRINT) :
          jsonpp(json_encode($this->display->get_option('qtip')))
      ) : '',
    );
  }

  function options_validate(&$form, &$form_state) {
    $settings = json_decode($form_state['values']['qtip']);
    if (json_last_error() != JSON_ERROR_NONE) {
      form_set_error('qtip', t(json_last_error_msg()));
    }
  }

  function options_submit(&$form, &$form_state) {
    $settings = json_decode($form_state['values']['qtip']);
    $this->display->set_option('qtip', $settings);
  }

  function options_summary(&$categories, &$options) {
    $options['qtip'] = array(
      'category' => 'other',
      'title' => t('qTip settings'),
      'value' => t('Settings'),
      'desc' => t('Change the qTip settings for this display.'),
    );
  }
}

// @see http://us3.php.net/manual/en/function.json-last-error-msg.php#113243
if (!function_exists('json_last_error_msg')) {
  function json_last_error_msg() {
    static $errors = array(
      JSON_ERROR_NONE             => null,
      JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
      JSON_ERROR_STATE_MISMATCH   => 'Underflow or the modes mismatch',
      JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
      JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
      JSON_ERROR_UTF8             => 'Malformed UTF-8 characters, possibly incorrectly encoded'
    );
    $error = json_last_error();
    return array_key_exists($error, $errors) ? $errors[$error] : "Unknown error ({$error})";
  }
}

// @see http://ryanuber.com/07-10-2012/json-pretty-print-pre-5.4.html
if (!function_exists('jsonpp')) {
  /**
   * jsonpp - Pretty print JSON data
   *
   * In versions of PHP < 5.4.x, the json_encode() function does not yet provide a
   * pretty-print option. In lieu of forgoing the feature, an additional call can
   * be made to this function, passing in JSON text, and (optionally) a string to
   * be used for indentation.
   *
   * @param string $json  The JSON data, pre-encoded
   * @param string $istr  The indentation string
   *
   * @return string
   */
  function jsonpp($json, $istr='  ')
  {
    $result = '';
    for($p=$q=$i=0; isset($json[$p]); $p++)
    {
      $json[$p] == '"' && ($p>0?$json[$p-1]:'') != '\\' && $q=!$q;
      if(strchr('}]', $json[$p]) && !$q && $i--)
      {
        strchr('{[', $json[$p-1]) || $result .= "\n".str_repeat($istr, $i);
      }
      $result .= $json[$p];
      if(strchr(',{[', $json[$p]) && !$q)
      {
        $i += strchr('{[', $json[$p])===FALSE?0:1;
        strchr('}]', $json[$p+1]) || $result .= "\n".str_repeat($istr, $i);
      }
    }
    return $result;
  }
}
